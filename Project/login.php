<!--
  Mattia Salasso Tweb 2018-19
  pagina dove un utente può autenticarsi
-->

<?php include("top.php"); ?>
<?php if(!isset($_SESSION)){ session_start(); } ?>

<?php
  if(!isset($_SESSION["email"])){ ?>
    <div id="login-signup">
      <form id="login" action="server/login-submit.php" method="post">
        <h2>Inserisci i tuoi dati</h2>
        <div class="form-group">
          <label>Indirizzo email</label>
          <input id="email" type="email" class="form-control" name="email" placeholder="Email" required>
          <?php
            if(isset($_SESSION["errormail"])){ ?>
              <small class="form-text text">E-mail non registrata</small><?php
              unset($_SESSION["errormail"]);
            }
          ?>
        </div>

        <div class="form-group">
          <label>Password</label>
          <input id="password" type="password" class="form-control" maxlength="16" name="password" placeholder="Password" required>
          <?php
            if(isset($_SESSION["errorpassword"])){ ?>
              <small class="form-text text">Password errata</p><?php
              unset($_SESSION["errorpassword"]);
            }else{ ?>
              <small class="form-text text-muted">Non dovresti comunicare a nessuno la tua password</small><?php
            }
          ?>
        </div>

        <div class="form-group">
          <button type="submit" class="btn btn-primary">Login</button>
        </div>

        <div class="form-group">
          <a href="signup.php">Nuovo su My Shopp? Registrati ora</a>
        </div>
      </form><!-- #login -->
    </div><?php #login-signup
  }else{ ?>
    <div class="container">
      <h1>Login già effettuato</h1>
      <a class="nav-link" href="showAllProduct.php">Inizia la ricerca di articoli</a>
    </div><?php
  }
?>

<script src="JS/login.js"></script>

<?php include("bottom.php"); ?>
