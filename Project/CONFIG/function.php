<?php include("config.php"); ?>
<?php if(!isset($_SESSION)){ session_start(); } ?>

<?php
  function signUp($user,$mail,$password) {
    try {
      $db = connectToDatabase();
      $user = $db->quote($user);
      $mail = $db->quote($mail);
      $password = md5($password);
      $password = $db->quote($password);
      $query = "INSERT INTO users(email,username,password) VALUES ($mail,$user,$password);";
      $rows = $db->query($query);
      return $rows;
    }catch(PDOException $ex) { ?>
      <p>Sorry, a database error occurred.</p><?php
      return NULL;
    }
  }

  function mailCorrect($mail){
    $db = connectToDatabase();
    $rows = $db->query("SELECT email FROM users");
    foreach($rows as $row){
      if(strcmp($row["email"],$mail) == 0){
        return true;
      }
    }
    return false;
  }

  function passwordCorrect($mail,$password){
      $db = connectToDatabase();

      $mail = $db->quote($mail);
      $password = md5($password);
      $password = $db->quote($password);

      $rows = $db->query("SELECT password FROM users WHERE email = $mail");
      if($rows){
        foreach ($rows as $row){
            $correct_password = $row["password"];
            $correct_password = $db->quote($correct_password);
            return $password == $correct_password;
        }
      }else{
        return FALSE;   #user not found
      }
    }

    function increaseViews($views,$description){ #incrementa le visualizzazioni ad ogni visualizzazione dell'articolo in dettaglio
      $db = connectToDatabase();
      $viewsQuote = $db->quote($views);
      $descriptionQuote = $db->quote($description);
      $incr = $db->exec("UPDATE products SET views = $viewsQuote WHERE description = $descriptionQuote");
    }

    function similarArticles($type,$brand){ // restituisce gli articoli simili
      $db = connectToDatabase();
      $type = $db->quote($type);
      $brand = $db->quote($brand);
      $rows = $db->query("SELECT * FROM products WHERE brand = $brand AND type = $type ORDER BY RAND()");
      return $rows;
    }

    function mostViews(){ // restituisce gli articoli piu visti
      $db = connectToDatabase();
      $rows = $db->query("SELECT * FROM products ORDER BY views DESC LIMIT 4");
      return $rows;
    }

    function articlesSelected($id){ // ritorna l'articolo in posizione $i in SESSION["truck"]
      $db = connectToDatabase();
      $item = $db->quote($id);
      $query = "SELECT * FROM products WHERE id = $item";
      $rows = $db->query($query);
      return $rows;
    }

    function requestType($type){ // ritorna gli articoli di categoria $type
      $db = connectToDatabase();
      $type = $db->quote($type);
      $rows = $db->query("SELECT * FROM products WHERE type = $type ORDER BY RAND()");
      return $rows;
    }

    function requestAllCategory(){ // ritorna tutti gli articoli
      $db = connectToDatabase();
      $rows = $db->query("SELECT * FROM products ORDER BY RAND()");
      return $rows;
    }

    function requestMenuCategory($category){ // ritorna le categorie diverse da $category
      $db = connectToDatabase();
      $category = $db->quote($category);
      $rows = $db->query("SELECT DISTINCT type FROM products WHERE type <> $category");
      return $rows;
    }

?>
