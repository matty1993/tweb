$("pay-button").observe("click",verifyInformation); // listener del button "paga"

function verifyInformation(event){ // Verifica sui campi di input,successivamente acquista
  if(nameControl($("name").value) && numberControl($("number-card").value) && exspirationControl($("exspiration").value)
      && securityControl($("code").value) && addressControl($("address").value)){
    buyArticle();
    showInfoTruck("success");
  }
}

function buyArticle(){ // comunica al server di togliere la quantità degli articoli che sono stati acquistati
  new Ajax.Request("XML/truck.php",
      {
          method: "POST",
          parameters: {command: "buy"},
          onFailure: ajaxFailed,
          onException: ajaxFailed
      }
  );
}

function nameControl(name){
  var expression = /^([a-z\u00C0-\u00F6\u00F8-\u017E]{2,})(\s[a-z\u0027\u00C0-\u00F6\u00F8-\u017E]{2,}){1,3}$/i;
  if($("errorName") != null){
    $("errorName").remove();
  }
  if(!expression.test(name)){
    var errorName = document.createElement("small");
    errorName.id = "errorName";
    errorName.className = "form-text text";
    errorName.innerHTML = "Inserire un nome valido";
    $("name").parentNode.appendChild(errorName);
    $("name").clear();
    return false;
  }
  return true;
}

function numberControl(numberCard){
  var expression = /^[0-9]{16}$/i;
  if($("errorNumber") != null){
    $("errorNumber").remove();
    var number = document.createElement("small");
    number.className = "form-text text-muted";
    number.innerHTML = "Il numero deve essere composto da 16 cifre";
    $("number-card").parentNode.appendChild(number);
  }
  if(!expression.test(numberCard)){
    var errorNumber = document.createElement("small");
    errorNumber.id = "errorNumber";
    errorNumber.className = "form-text text";
    errorNumber.innerHTML = "Errore,il numero inserito non contiene il numero esatto di cifre";
    $("number-card").nextSiblings()[0].remove();
    $("number-card").parentNode.appendChild(errorNumber);
    $("number-card").clear();
    return false;
  }
  return true;
}

function exspirationControl(exspiration){
  var date;
  var expression = /^[0-9]{2}\/[0-9]{2}$/i;
  if($("errorDate") != null){
    $("errorDate").remove();
  }

  if(expression.test(exspiration)){
    var month = parseInt($("exspiration").value.substring(0,2));
    var year = parseInt($("exspiration").value.substring(3,5));

    if(1 <= month && month <= 12){
      if(year < 19){ //year error
        var errorYear = document.createElement("small");
        errorYear.id = "errorDate";
        errorYear.className = "form-text text";
        errorYear.innerHTML = "Anno non valido";
        $("exspiration").parentNode.appendChild(errorYear);
        $("exspiration").clear();
        return false;
      }
    }else{ //month error
      var errorMonth = document.createElement("small");
      errorMonth.id = "errorDate";
      errorMonth.className = "form-text text";
      errorMonth.innerHTML = "Mese non valido";
      $("exspiration").parentNode.appendChild(errorMonth);
      $("exspiration").clear();
      return false;
    }
  }else{
    var errorDate = document.createElement("small");
    errorDate.id = "errorDate";
    errorDate.className = "form-text text";
    errorDate.innerHTML = "Formato non valido";

    $("exspiration").parentNode.appendChild(errorDate);
    $("exspiration").clear();
    return false;
  }
  return true;
}

function securityControl(number){
  var expression = /^[0-9]{3}$/i;
  if($("errorCode") != null){
    $("errorCode").remove();
    var code = document.createElement("small");
    code.className = "form-text text-muted";
    code.innerHTML = "Codice di 3 cifre";
    $("code").parentNode.appendChild(code);
  }
  if(!expression.test(number)){
    var errorCode = document.createElement("small");
    errorCode.id = "errorCode";
    errorCode.className = "form-text text";
    errorCode.innerHTML = "Codice errato";
    $("code").nextSiblings()[0].remove();
    $("code").parentNode.appendChild(errorCode);
    $("code").clear();
    return false;
  }
  return true;
}

function addressControl(addressName){
  var expression = /^([a-z- ]+)\s+([0-9]+)$/i;
  if($("errorAddress") != null){
    $("errorAddress").remove();
    var address = document.createElement("small");
    address.className = "form-text text-muted";
    address.innerHTML = "Inserire nome della via e numero civico";
    $("address").parentNode.appendChild(address);
  }
  if(!expression.test(addressName)){
    var errorAddress = document.createElement("small");
    errorAddress.id = "errorAddress";
    errorAddress.className = "form-text text";
    errorAddress.innerHTML = "Formato errato, inserire (Nome via + Numero). es: torino 28";
    $("address").nextSiblings()[0].remove();
    $("address").parentNode.appendChild(errorAddress);
    $("address").clear();
    return false;
  }
  return true;
}
