for(var i=0; i<$$("#categories li input").length; i++){ // listener delle categorie sulla home page
  $$("#categories li input")[i].onclick = articlesClick;
}

function articlesClick(){ // richiede una determinata categoria di articoli
  new Ajax.Request("XML/articles_xml.php",
      {
          method: "GET",
          parameters: {category: this.value},
          onSuccess: showArticles,
          onFailure: ajaxFailed,     // function shown in previous sections
          onException: ajaxFailed
      }
  );
}

function showArticles(ajax){ // mostra articoli a seconda della categoria cliccata
  while ($("mainDeck").firstChild) {
       $("mainDeck").removeChild($("mainDeck").firstChild);
  }

  var products = ajax.responseXML.getElementsByTagName("product");

  for(var i=0; i<products.length; i++){
    if(i == 3){
      break;
    }
    createCard(products[i]);
  }

  addListener(); // aggiunge i listener ai button delle nuove card

}
