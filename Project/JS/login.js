$("login").observe("submit",clientLoginControl); // controllo client-side della password

function clientLoginControl(event){
  if($("errorPassword") != null){
    $("errorPassword").remove();
  }

  if($("password").next() != null){
    $("password").next().remove();
  }

  if($("password").value.includes("<") || $("password").value.includes(">")){
    var errorPassword = document.createElement("small");
    errorPassword.id = "errorPassword";
    errorPassword.className = "form-text text";
    errorPassword.innerHTML = "Caratteri non validi";
    $("password").parentNode.appendChild(errorPassword);
    event.preventDefault();
  }
}
