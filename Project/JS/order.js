$("menu").observe("change",menu); //listener del menu a tendina

for(var i=0; i<$("choose").querySelectorAll("input").length; i++){ // listener degli input radio
  $("choose").querySelectorAll("input")[i].onclick = orderClick;
}

function menu(){
  $("categoria").innerHTML = this.value;
  changeCategory();
}

function changeCategory(){ // mostra la categoria selezionata dal menu
  new Ajax.Request("XML/articles_xml.php",
    {
        method: "GET",
        parameters: {category: $("menu").value},
        onSuccess: showOrderedArticles,
        onFailure: ajaxFailed,     // function shown in previous sections
        onException: ajaxFailed
    }
  );
}

function orderClick(){ // ordina gli articoli,(prezzo o valutazione)
  var articles = $$("#showAll .card");
  checkError();

  if($("price").checked == true){
    orderArticles("price",articles);
  }else if($("rating").checked == true){
    orderArticles("rating",articles);
  }

}

function orderArticles(str,articles){
  switch(str){
    case "price":
      articles.sort(function(a,b){
        return parseInt(a.getElementsByClassName("price")[0].innerHTML) - parseInt(b.getElementsByClassName("price")[0].innerHTML);
      });
      order(articles);
      break;
    case "rating":
      articles.sort(function(a,b){
        return (b.getElementsByClassName("rating")[0].children.length) - (a.getElementsByClassName("rating")[0].children.length);
      });
      order(articles);
      break;
  }
}

function showOrderedArticles(ajax){ // ordina gli articoli a seconda della cateforia selezionata
  remove();
  cardDeckAppend();
  var products = ajax.responseXML.getElementsByTagName("product");
  show(products,"menu");
  check();
}

function order(articles){
  remove();
  cardDeckAppend();
  show(articles,"radio");
}

function show(products,option){ // mostra gli articoli scelti, in base alla categoria o alla scelta("prezzo" o "valutazione")
  for(var i=0; i<products.length; i++){
    if(i%3 == 0){
      var card_deck = document.createElement("div");
      card_deck.className = "card-deck allCardDeck";
      card_deck.id = "mainDeck";
      $("append").appendChild(card_deck);
    }

    if(i%3 == 0 && i != 0){
      $("mainDeck").id = "";
    }

    if(option == "radio"){
      $("mainDeck").appendChild(products[i]);
    }else{
      createCard(products[i]);
    }
  }

  addListener();

}

function check(){
  var checked = $("choose").querySelectorAll("input");
  for(var j=0; j<checked.length; j++){
    if(checked[j].checked == true){
      checked[j].checked = false;
    }
  }
}
