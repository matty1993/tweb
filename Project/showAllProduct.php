<!--
  Mattia Salasso Tweb 2018-19
  pagina dove è possibile vedere tutti gli articoli ed ordinarli o cercarli
-->

<?php include("top.php"); ?>
<?php if(!isset($_SESSION)){ session_start(); } ?>

<?php
  if(isset($_REQUEST["type"])){
    $category = $_REQUEST["type"];
    $rows = requestType($category);
  }else{
    $category = "tutte le categorie";
    $rows = requestAllCategory();
  }
?>

<div id="showAll" class="container">

  <nav class="navbar navbar-expand-lg bg-dark">
    <a id="categoria" class="navbar-brand"><?= $category ?></a>

    <div class="collapse navbar-collapse">
      <a>Ordina per: </a>
      <ul id="choose" class="navbar-nav mr-auto mt-2 mt-lg-0">
        <li class="nav-item active">
          <input id="price" type="radio" name="choose" value="price">Prezzo
        </li>
        <li class="nav-item active">
          <input id="rating" type="radio" name="choose" value="value">Valutazione
        </li>
        <li class="nav-item active">
          <select id="menu">
            <option value="<?= $category ?>"><?= $category ?></option>
            <?php
              $lines = requestMenuCategory($category);
              foreach($lines as $line){ ?>
                <option value="<?= $line["type"] ?>"><?= $line["type"] ?></option><?php
              }
            ?>
          </select>
        </li>
      </ul>

      <div class="form-inline">
        <input id="name" class="form-control mr-sm-2" type="search" placeholder="Cerca un prodotto" aria-label="Search">
        <button id="search" class="btn btn-outline-success my-2 my-sm-0">Cerca</button>
      </div>
    </div>
  </nav>

  <div class="card-deck allCardDeck">
  <?php
    $i = 0;
    foreach($rows as $line){
      if($i%3 == 0 && $i != 0){ ?>
        </div>
        <div class="card-deck allCardDeck"><?php
      }
    ?>
      <div class="card">
        <img src=<?= $line["image"] ?> class="card-img-top" alt="card image">
        <div class="card-body">
          <ul class="list-group list-group-flush">
            <li class="list-group-item"><h4 class="card-title type"><?= $line["type"] ?></h4>
            <li class="list-group-item"><h5 class="card-title"><?= $line["brand"] ?></h5>
            <li class="list-group-item"><p class="card-text"><?= $line["description"] ?></p></li>
            <li class="list-group-item"><p class="card-text price"><?= $line["price"] ." €" ?></p></li>
            <li class="list-group-item"><p class="card-text"><?= $line["qty"] ." articoli disponibili"?></p></li>
            <li class="list-group-item">
              <p class="card-text rating">
                <?php
                  $rating = $line["rating"];
                  for($j=0; $j<$rating; $j++){ ?>
                    <i class="fa star">&#xf005;</i><?php
                  }
                ?>
              </p>
            </li>
          </ul>
        </div>

        <div class="card-footer">
          <form action="product.php" method="post">
            <input type="hidden" name="id" value="<?= $line["id"]; ?>" >
            <input type="hidden" name="image" value="<?= $line["image"]; ?>" >
            <input type="hidden" name="type" value="<?= $line["type"]; ?>" >
            <input type="hidden" name="description" value="<?= $line["description"]; ?>" >
            <input type="hidden" name="brand" value="<?= $line["brand"]; ?>" >
            <input type="hidden" name="price" value="<?= $line["price"]; ?>" >
            <input type="hidden" name="qty" value="<?= $line["qty"]; ?>" >
            <input type="hidden" name="rating" value="<?= $line["rating"]; ?>" >
            <input type="hidden" name="views" value="<?= $line["views"]; ?>" >
            <button type="submit" class="btn btn-primary">Visualizza</button>
            <a class="btn btn-primary view add">Aggiungi al carrello</a>
          </form>
        </div>
      </div><?php # .card
        $i++;
    }
  ?>
  </div><!-- .allCardDeck -->
</div><!-- #showAll -->

<script src="JS/order.js"></script>
<script src="JS/search.js"></script>

<?php include("bottom.php"); ?>
