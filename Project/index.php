<!--
  Mattia Salasso Tweb 2018-19
  pagina principale, presenta una lista di articoli ed una lista di articoli più visti
-->

<?php include("top.php"); ?>
<?php if(!isset($_SESSION)){ session_start(); } ?>

<div id="showAll" class="row main">
  <div class="col-sm-2 col-nav">
    <nav class="navbar navbar-light bg-dark">
      <div class="container">
        <a class="navbar-brand">Categorie</a>
      </div>
    </nav>
    <div class="container">
      <h5>Scegli una categoria:</h5>
      <ul id="categories"><!-- list categories -->
        <li><input type="radio" name="choose" value="computer">computer</li>
        <li><input type="radio" name="choose" value="tv">tv</li>
        <li><input type="radio" name="choose" value="telephone">telephone</li>
        <li><input type="radio" name="choose" value="watches">watches</li>
      </ul>
    </div>
  </div>

  <div class="col-sm-10 col-nav">
    <nav class="navbar navbar-light bg-dark">
      <div class="container">
        <a class="navbar-brand">Articoli</a>
      </div>
    </nav>

    <div id="mainDeck" class="card-deck allCardDeck">
    <?php
      $lines = file("TXT/view.txt",FILE_IGNORE_NEW_LINES);
      foreach($lines as $line){
        list($type,$description,$path) = explode(",",$line); ?>
        <div class="card">
          <img src=<?= $path ?> class="card-img-top" alt="card image">
          <div class="card-body">
            <h5 class="card-title"><?= $type ?></h5>
            <p class="card-text"><?= $description ?></p>
          </div>
          <div class="card-footer">
            <form action="showAllProduct.php" method="post">
              <input type="hidden" name="type" value="<?= $type ?>">
              <button type="submit" class="btn btn-primary view">Visualizza</button>
            </form>
          </div>
        </div><?php
      }
    ?>
    </div> <!-- #mainDeck -->
  </div><!-- .col-sm-10 col-nav -->
</div><!-- #showAll -->

<div id="mostView">
  <div class="col-sm-12 col-nav">
    <nav class="navbar navbar-light bg-dark">
      <div class="container">
        <a class="navbar-brand">Articoli più visti</a>
      </div>
    </nav>

    <div class="card-deck">
      <?php
        $rows = mostViews();
        foreach($rows as $line){ ?>
          <div class="card">
            <img src=<?= $line["image"] ?> class="card-img-top" alt="card image">
            <div class="card-body">
              <p class="card-text"><?= $line["description"] ?></p>
            </div>
            <div class="card-footer">
              <form action="product.php" method="post">
                <input type="hidden" name="id" value="<?= $line["id"]; ?>" >
                <input type="hidden" name="image" value="<?= $line["image"]; ?>" >
                <input type="hidden" name="type" value="<?= $line["type"]; ?>" >
                <input type="hidden" name="description" value="<?= $line["description"]; ?>" >
                <input type="hidden" name="brand" value="<?= $line["brand"]; ?>" >
                <input type="hidden" name="price" value="<?= $line["price"]; ?>" >
                <input type="hidden" name="qty" value="<?= $line["qty"]; ?>" >
                <input type="hidden" name="rating" value="<?= $line["rating"]; ?>" >
                <input type="hidden" name="views" value="<?= $line["views"]; ?>" >
                <button type="submit" class="btn btn-primary view">Visualizza</button>
              </form>
            </div>
          </div><?php
        }
      ?>
    </div><!-- .cardDeck -->
  </div><!-- .col-sm-12 col-nav -->
</div><!-- #mostView -->

<script src="JS/articles.js"></script>

<?php include("bottom.php"); ?>
